<?php

use Faker\Generator as Faker;
use App\Autor;

$factory->define(App\Libro::class, function (Faker $faker) {
	$autor = Autor::inRandomOrder()->first();
    return [
        'titulo' => $faker->catchPhrase,
        'autor_id' => $autor->id,
        'fecha_publicacion' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'tipo' => $faker->word,
        'descripcion' => $faker->realText($maxNbChars = 200),
    ];
});
