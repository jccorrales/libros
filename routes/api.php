<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('autores', 'AutorController@listarAutores');
Route::get('autores/{autor}', 'AutorController@obtenerAutor');
Route::get('libros', 'LibroController@listarLibros');
Route::get('libros/{libro}', 'LibroController@obtenerLibro');
Route::get('autores/{autor}/libros', 'AutorController@listarLibrosAutor');
Route::post('autores', 'AutorController@agregarAutor');
Route::post('libros', 'LibroController@agregarLibro');
Route::put('autores/{autor}', 'AutorController@actualizarDatosAutor');
Route::put('libros/{libro}', 'LibroController@actualizarDatosLibro');
Route::delete('autores/{autor}', 'AutorController@eliminarAutor');
Route::delete('libros/{libro}', 'LibroController@eliminarLibro');