<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    protected $fillable = [
    	'nombre',
    	'nacionalidad',
    	'bio',
    ];

    protected $table = 'autores';

    public function libros() {
    	return $this->hasMany('App\Libro');
    }
}