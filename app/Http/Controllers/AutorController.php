<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Autor;

class AutorController extends Controller
{
    public function listarAutores() {

    	$autores = Autor::all();

    	return response()->json($autores, 200);
    }

    public function obtenerAutor($id) {

    	$autor = Autor::find($id);

    	if($autor) {
    		return response()->json($autor, 200);
    	}

    	return response()->json(['mensaje' => 'No se encontró el recurso especificado'], 404);
    }

    public function listarLibrosAutor($id) {

    	$autor = Autor::find($id);

    	if(!$autor) {
    		return response()->json(['mensaje' => 'No se encontró el autor especificado'], 404);
    	}

    	if($autor->libros->isEmpty()) {
    		return response()->json(['mensaje' => 'No se encontraron libros pertenecientes a este autor'], 404);
    	}

    	return response()->json($autor->libros, 200);
    }

    public function agregarAutor(Request $request) {

    	$request->validate([
    		'nombre' 	   => 'string|required',
    		'nacionalidad' => 'string|required',
    		'bio' 		   => 'string|required',
    	]);

    	$autor = new Autor([
    		'nombre' => $request->nombre,
    		'nacionalidad' => $request->nacionalidad,
    		'bio' => $request->bio,
    	]);

    	$autor->save();

    	return response()->json(['mensaje' => 'Autor agregado exitosamente'], 201);
    }

    public function actualizarDatosAutor(Request $request, $id) {

    	$request->validate([
    		'nombre' => 'string|required',
    		'nacionalidad' => 'string|required',
    		'bio' => 'string|required',
    	]);

    	$autor = Autor::find($id);

    	if(!$autor) {
    		return response()->json(['mensaje' => 'No se encontró el recurso solicitado'], 404);
    	}

    	$autor->nombre = $request->nombre;
    	$autor->nacionalidad = $request->nacionalidad;
    	$autor->bio = $request->bio;

    	$autor->save();
    	
    	return response()->json([
    		'mensaje' => 'Datos modificados exitosamente',
    		'autor' => url('/api/autores/'.$autor->id),
    	], 200);
    }

    public function eliminarAutor($id) {

    	$autor = Autor::find($id);

    	if(!$autor) {
    		return response()->json(['mensaje' => 'No se encontró el recurso solicitado'], 404);
    	}

    	$autor->delete();

    	return response()->json(['mensaje' => 'Datos eliminados con éxito'], 200);
    }
}
