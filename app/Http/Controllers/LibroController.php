<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libro;

class LibroController extends Controller
{
    public function listarLibros() {

    	$libros = Libro::all();
    	return response()->json($libros, 200);
    }

    public function obtenerLibro($id) {

    	$libro = Libro::find($id);

    	if($libro) {
    		return response()->json($libro, 200);
    	}

    	return response()->json(['mensaje' => 'No se encontró el recurso especificado'], 404);
    }

    public function agregarLibro(Request $request) {

    	$request->validate([
    		'titulo' => 'string|required',
    		'autor_id' => 'integer|required|exists:autores,id',
    		'fecha_publicacion' => 'date_format:Y-m-d|required',
    		'tipo' => 'string|required',
    		'descripcion' => 'string|required',
    	]);

    	$libro = new Libro([
    		'titulo' => $request->titulo,
    		'autor_id' => $request->autor_id,
    		'fecha_publicacion' => $request->fecha_publicacion,
    		'tipo' => $request->tipo,
    		'descripcion' => $request->descripcion,
    	]);

    	$libro->save();

    	return response()->json(['mensaje' => 'Libro agregado exitosamente'], 201);
    }

    public function actualizarDatosLibro($id) {

    	$request->validate([
    		'titulo' => 'string|required',
    		'autor_id' => 'integer|required|exists:autores,id',
    		'fecha_publicacion' => 'date_format:Y-m-d|required',
    		'tipo' => 'string|required',
    		'descripcion' => 'string|required',
    	]);

    	$libro = Libro::find($id);

    	if(!$libro) {
    		return response()->json(['mensaje' => 'No se encontró el recurso solicitado'], 404);
    	}

    	$libro->titulo = $request->titulo;
    	$libro->autor_id = $request->autor_id;
    	$libro->fecha_publicacion = $request->fecha_publicacion;
    	$libro->tipo = $request->tipo;
    	$libro->descripcion = $request->descripcion;

    	$libro->save();
    	
    	return response()->json([
    		'mensaje' => 'Datos modificados exitosamente',
    		'libro' => url('/api/libros/'.$libro->id),
    	]);
    }

    public function eliminarLibro($id) {

    	$libro = Libro::find($id);

    	if(!$libro) {
    		return response()->json(['mensaje' => 'No se encontró el recurso solicitado'], 404);
    	}

    	$libro->delete();

    	return response()->json(['mensaje' => 'Datos eliminados con éxito'], 200);
    }
}