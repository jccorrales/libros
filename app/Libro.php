<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    protected $fillable = [
    	'titulo',
    	'autor_id',
    	'fecha_publicacion',
    	'tipo',
    	'descripcion',
    ];

    public function autor() {
    	return $this->belongsTo('App\Autor');
    }
}
